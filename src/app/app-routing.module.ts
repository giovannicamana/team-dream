import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './core/pages/landing/landing.component';


const routes: Routes = [
  {
    path: '',
    component: LandingComponent,
    data: { animation: 'routing' }
  },
  {
    path: 'main',
    loadChildren: 'app/modules/main/main.module#MainModule'
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
