import { NgModule, ModuleWithProviders, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoreRoutingModule } from './core-routing.module';
import { CommonService } from './common.service';
import { HeadersService } from './headers.service';
import { ConfigService } from './config.service';
import { LandingComponent } from './pages/landing/landing.component';
import { LoadingComponent } from './components/loading/loading.component';
import { ListBrandsComponent } from './components/list-brands/list-brands.component';
import { HttpClientModule } from '@angular/common/http';
import { TalentCardComponent } from './components/talent-card/talent-card.component';
import { SkillCircleComponent } from './components/skill-circle/skill-circle.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { ForgotComponent } from './components/forgot/forgot.component';

// import { IdentityService } from './providers/session/identity.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    CoreRoutingModule
  ],
  declarations: [
    LandingComponent,
    LoadingComponent,
    ListBrandsComponent,
    TalentCardComponent,
    SkillCircleComponent,
    LoginComponent,
    SignupComponent,
    ForgotComponent
  ],
  providers: [
    CommonService,
    HeadersService,
    ConfigService,
    // IdentityService,
  ],
  exports: [
    LoadingComponent
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import it in the AppModule only')
    }
  }
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule
    }
  }
}
