
// import { Injectable, EventEmitter } from '@angular/core';
// // import { UserStorageService } from '../../../shared/providers/storage/user-storage.service';
// import { SharedConstants } from './../../../shared/shared.constants';

// @Injectable()
// export class IdentityService {

//     private _user: any;
//     private _token: string;

//     subscriber: EventEmitter<any>;

//     constructor(private _userStorageSrv: UserStorageService) {
//         this.subscriber = new EventEmitter<any>();
//     }

//     set user(user: any) {
//         if (user !== undefined) {
//             this._userStorageSrv.setCurrentUser(user);
//         } else {
//             this._userStorageSrv.removeCurrentUser();
//         }
//         this._user = user;
//         this.subscriber.next(user);
//     }

//     get user(): any {
//         return this._user;
//     }

//     set token(token: string) {
//         if (token) this._userStorageSrv.setToken(token);
//         else this._userStorageSrv.removeToken();

//         this._token = token;
//     }

//     get token(): string {
//         return this._token;
//     }

//     get isAdmin(): boolean {
//         return this._user.role === 'admin';
//     }

//     getProfileImage(): string {
//         return this._user.profileImage || SharedConstants.PATHS.UNKNOWN_USER_IMAGE;
//     }

//     getCurrentUser(): Promise<any> {
//         let currentUser: any;

//         return this._userStorageSrv.getCurrentUser()
//             .then(user => {
//                 currentUser = user;
//                 return this._userStorageSrv.getToken();
//             })
//             .then(token => {
//                 this.token = token;
//                 this.user = currentUser;
//                 return currentUser;
//             });
//     }

//     isLoggedIn(): boolean {
//         return this._user !== undefined && this._token !== undefined;
//     }

//     logout(): void {
//         this.token = undefined;
//         this.user = undefined;

//         this._userStorageSrv.clearAll();

//         this.subscriber.next(this.user);
//     }

// }
