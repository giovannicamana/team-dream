import { Injectable } from "@angular/core";

let ENV = 'local';
let IP = '192.168.8.101';
let PORT = 3000;

@Injectable()

export class ConfigService {
    BASE_URL: string;
    constructor() {
        this.BASE_URL = this._getBaseUrl();
    }

    private _getBaseUrl(): string {
        let urls = {
            local: `http://localhost:${PORT}/`,
            dev: 'https://api.github.com/',
            production: ''
        };
        return urls[ENV];
    }
}