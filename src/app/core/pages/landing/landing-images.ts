export default [
    {
        title: 'Busca en la BD de talento unica',
        src: "/assets/images/1.svg",
        description: "TuEquipo le da acceso a ingenieros disponibles para trabajos dedicados de contratistas en las principales firmas de consultoría de TI.",
        description1: "No puede encontrarlos en portales independientes o bolsas de trabajo."
    },
    {
        title: 'Obten el trabajo comenzado en dias',
        src: "/assets/images/2.svg",
        description: "No es necesario gastar meses y miles de dólares en reclutamiento.",
        description1: "Su nuevo compañero de equipo a tiempo completo está listo para comenzar el próximo lunes."
    },
    {
        title: 'Disfruta de una cooperación estable y sin complicaciones',
        src: "/assets/images/3.svg",
        description: "La nómina, el lugar de trabajo, las vacaciones, la capacitación, el área legal y otras cuestiones administrativas están a cargo de una empresa de consultoría de TI confiable en la ubicación del ingeniero.",
        description1: "El modelo dedicado garantiza que el ingeniero trabaje exclusivamente en su proyecto."
    },
    {
        title: 'Sentirse Seguro',
        src: "/assets/images/4.svg",
        description: "Un marco legal robusto asegura que su IP esté protegida.",
        description1: "TuEquipo también ofrece una garantía de devolución de dinero para toda la duración del proyecto."
    }
]