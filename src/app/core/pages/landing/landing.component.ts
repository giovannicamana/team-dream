import { Component, OnInit } from '@angular/core';
import { SharedConstants } from '../../../shared/shared.constants';

import PHOTOS from './landing-images';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  LOGO: string = SharedConstants.PATHS.LOGO;
  date: Date = new Date();
  isSearch: boolean = false;
  isActiveLogin: boolean = false;
  user: any;

  imagesAvatar: IPhoto[] = [];

  constructor(
    private _httpClient: HttpClient
  ) { }

  ngOnInit() {
    this.imagesAvatar = PHOTOS;
  }

  goToLogin() {
    this.isActiveLogin = !this.isActiveLogin;
  }
  onToggleLogin(event){
    this.isActiveLogin = event;
  }

  search() {
    this.isSearch = true;
  }
}

interface IPhoto {
  title: string;
  src: string;
  description: string;
  description1: string;
}