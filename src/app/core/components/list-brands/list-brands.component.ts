import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-brands',
  templateUrl: './list-brands.component.html',
  styleUrls: ['./list-brands.component.scss']
})
export class ListBrandsComponent implements OnInit {
  images: any[] = [];
  constructor() { }

  ngOnInit() {
    this.images = [
      "/assets/gallery/basecamp.svg",
      "/assets/gallery/buffer.svg" ,
      "/assets/gallery/grubhut.svg",
      "/assets/gallery/pocket.svg" ,
      "/assets/gallery/trello.svg",
    ];
  }
}
