import { trigger, transition } from '@angular/animations';
import { Component, OnInit, HostBinding } from '@angular/core';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css'],
  animations: [
    trigger('pageAnimations', [
      transition(':enter', [
        // animation for the page entering
      ]),
      transition(':leave', [
        // animation for the page leaving
      ])
    ])
  ]
})
export class LoadingComponent implements OnInit {

  constructor() { }

  @HostBinding('@pageAnimations')
  public animatePage = true;

  ngOnInit() {
  }

}
