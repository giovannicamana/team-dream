import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { SharedConstants } from '../../../shared/shared.constants';
import { Location } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  @Input() isActive: boolean = false;
  @Output() onToggle: EventEmitter<boolean> = new EventEmitter<boolean>();
  isPassword: boolean;
  LOGO: string = SharedConstants.PATHS.LOGO;
  constructor(
    private _location:Location
  ) { }

  ngOnInit() {

  }

  dismiss() {
    this.isActive = false;
    if (!this.isActive) {
      this.onToggle.emit(this.isActive);
    }
  }

  viewPassword() {
    this.isPassword = !this.isPassword;
  }
  back() {
    this._location.back();
  }
}
