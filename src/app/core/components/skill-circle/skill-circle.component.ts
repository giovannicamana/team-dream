import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-skill-circle',
  templateUrl: './skill-circle.component.html',
  styleUrls: ['./skill-circle.component.scss']
})
export class SkillCircleComponent implements OnInit {
  @Input() year: number;
  @Input() title: string;
  radio: string = "";
  constructor() {
  }
  ngOnChanges() {
    this.radio = `p${this.year * 10}`;
  }
  ngOnInit() {
  }

}
