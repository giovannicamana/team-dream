import { Component, HostBinding, ChangeDetectorRef, ViewChild, SimpleChanges } from '@angular/core';
import { Router, RouterOutlet, NavigationStart } from '@angular/router';
import { transition, trigger, group, animate, style, query, animateChild } from '@angular/animations';

import PHOTOS from './photos';
import { SharedConstants } from './shared/shared.constants';

const MIN_PAGE_TIMEOUT = 1000;
const ELASTIC_BEZIER = 'cubic-bezier(.26,1.96,.58,.61)';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('loadingAnimation', [
      transition(':enter', [
        query('.loading-text', [
          style({ marginTop: '-200px' }),
          animate('1500ms ' + ELASTIC_BEZIER, style({ marginTop: '0px' }))
        ])
      ]),
      transition(':leave', [
        query('.loading-text', [
          animate('800ms ease-out', style({ opacity: '0' }))
        ]),
        animate('300ms', style({ opacity: 0 }))
      ])
    ]),
    trigger('routeAnimation', [
      transition('* => intro', [
        style({ position: 'relative' }),
        query(':enter, :leave', style({
          position: 'absolute', top: 0, left: 0, width: '100%'
        })),
        group([
          query(':enter', [
            style({ transform: 'translateX(-100px)', opacity: 0 }),
            animate('300ms ease-out', style({ opacity: 1, transform: 'none' })),
            animateChild()
          ]),
        ])
      ]),
      transition('* => advanced, * => routing, * => basics, * => programmatic, * => resources', [
        query(':enter', animateChild())
      ]),
      transition('* => *', [])
    ])
  ]
})

export class AppComponent {
  @HostBinding('@.disabled')
  animationsDisabled = false;
  MAQTA:string = SharedConstants.PATHS.MAQTA;
  // @ViewChild('tooltip')
  // public tooltip: ToolTipComponent;

  // @ViewChild('modal')
  // public modal: ModalComponent;

  public ready = false;
  private _preloaded = false;
  private _timeoutDone = false;
  public percentage = 0;

  constructor(
    private _cd: ChangeDetectorRef,
    private _router: Router,
  ) {
  }

  ngOnInit() {
    this.preloadPhotos(() => {
      this._preloaded = true;
      this.percentage = 100;
      this._onReady();
    }, (doneCount, totalCount) => {
      this.percentage = Math.ceil((doneCount / totalCount) * 100);
      this._cd.detectChanges();
    });

    setTimeout(() => {
      this._timeoutDone = true;
      this._onReady();
    }, MIN_PAGE_TIMEOUT);
  }

  private _onReady() {
    if (this._preloaded && this._timeoutDone) {
      this.ready = true;
      this._cd.detectChanges();
    }
  }

  preloadPhotos(onDoneCb: () => any, onProgressCb: (doneCount: number, totalCount: number) => any) {
    let count = 0;
    let done = false;
    const body = document.body;
    PHOTOS.forEach(photo => {
      const img = new Image();
      img.onload = onImageDone;
      img.src = photo.src;
    });

    function onImageDone() {
      if (!done && ++count >= PHOTOS.length) {
        done = true;
        onDoneCb();
      } else {
        onProgressCb(count, PHOTOS.length);
      }
    }
  }

  disableAnimations() {
    this.animationsDisabled = true;
  }

  enableAnimations() {
    this.animationsDisabled = false;
  }

  toggleAnimations() {
    this.animationsDisabled ? this.enableAnimations() : this.disableAnimations();
  }

  prepRouteAnimation(outlet: RouterOutlet) {
    return outlet.activatedRouteData['animation'] || '';
  }
}
