export default [
    { title: '1', src: "/assets/images/logo.svg", tags: ["logo mi equipo"] },
    { title: '2', src: "/assets/images/maqta.png", tags: ["maqta dev"] },

    { title: '3', src: "/assets/gallery/basecamp.svg", tags: ["logo mi equipo"] },
    { title: '4', src: "/assets/gallery/buffer.svg", tags: ["maqta dev"] },
    { title: '5', src: "/assets/gallery/grubhut.svg", tags: ["logo mi equipo"] },
    { title: '6', src: "/assets/gallery/pocket.svg", tags: ["maqta dev"] },
    { title: '7', src: "/assets/gallery/trello.svg", tags: ["logo mi equipo"] },

    { title: '8', src: "/assets/images/1.svg" },
    { title: '9', src: "/assets/images/2.svg"},
    { title: '10', src: "/assets/images/3.svg"},
    { title: '11', src: "/assets/images/4.svg"},
];
