export class SharedConstants {
    static get links() {
        return {
            GOOGLE_PLAY_URL: 'https://play.google.com/store?hl=en',
            ITUNES_URL: 'https://www.apple.com/itunes/'
        };
    }

    static map = { DEFAULT_MILES_RADIUS: 2, DEFAULT_ZOOM: 14 };

    static GOOGLEMAPS = {
        API_KEY: "AIzaSyD631Bb9E2LNPrUJuzxQb--thSNEItBGNU",
        DEFAULT_ZOOM: 14
    }

    static get messages() {
        return {
            error: {
                SELECT_USER: 'You must select a user type.',
                CONNECT_WITH_FACEBOOK: 'Connect with Facebook in a real device.',
                UPLOAD_FILE: 'Unable to upload the image. Try, again later.',
                RETRIEVING_DATA: 'Error retrieving data.',
                SAVING_DATA: 'Error Saving Data.',
                PHONE_NUMBER: 'No phone number provided.',
                PASSWORD_MATCH: 'Passwords must match.',
                USERNAME_EXISTS: 'Username already exists.',
                ENTER_REQUIRED_INFO: 'Please, enter all the required information.',
                ENTER_NMLS: 'You must enter a NMLS number.',
                ENTER_PHONE: 'You must enter a phone number.'
            },
            info: {
                LOGGING_IN: 'Logging in, please wait...',
                SAVING_DATA: 'Saving Data...'
            }
        };
    };
    static get PATHS() {
        return {
            LOGO: "./assets/images/logo.svg",
            MAQTA: "./assets/images/maqta.png",
            UNKNOWN_USER_IMAGE: 'assets/images/user/unknown-user.jpg',
            DEFAULT_BG_IMAGE: 'assets/images/profile-cover.jpg',
        };
    };
}